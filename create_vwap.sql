delimiter //
create procedure vwap(in startdate datetime, enddate datetime)
begin
	select date_format(dates, '%d/%m/%Y') as datee,
    time_format(startdate, '%H:%i') as start_time, time_format(enddate, '%H:%i') as end_time,
    round((SUM(vol*close)/SUM(vol)),4) as Volume_Weighted_Price
    from sd2
    where dates between startdate and enddate
    group by datee;
end //
delimiter ;