delimiter // 
create procedure day_return(IN dates date)
begin
	 select ticker, ((closeprice-openprice)/openprice)*100 as PercentGain
     from sd1
     where dates = dates
     order by PercentGain desc;
end //
delimiter ;