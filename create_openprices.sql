delimiter //
create procedure openprices(IN start float(5,2), end float(5,2))
begin
	declare limiter int;
    set limiter = 2;
    select * from sd1
    where open between start and end limit limiter;
end //
delimiter ;