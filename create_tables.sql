-- create tables & insert data

use ian;

drop table sd1;

create table sd1(
ticker varchar(9) primary key, dates date, openprice double(5,2), highprice double(5,2), 
lowprice double(5,2), closeprice double(5,2), vol int(10)
);

drop table sd2;

create table sd2(
ticker varchar(9), dates datetime, open double(10,6), high double(10,6),
low double(10,6), close double(10,6), vol int(10)
);

SELECT str_to_date(date, "%Y%m%d%H%i") FROM steve.sample2 limit 5;
insert into sd2
SELECT ticker, str_to_date(date, "%Y%m%d%H%i"), open, close, high, low, vol from steve.sample2;

SELECT * FROM sd2;

SELECT MAX(length(open)) FROM steve.sample2;

drop table sd3;


create table sd3(
date datetime, open double(10,6), close double(10,6), high double(10,6), low double(10,6),
vol int(10)
);

insert into sd3
select str_to_date(concat(date, ' ',time), "%m/%d/%Y %H%i"), open, high, low, close, volume FROM steve.sample3;