delimiter //
create procedure volatility()
begin
    create temporary table sd3_cleaned(
    date date, time time, open double(12,8), close double(12,8),
    high double(12,8), low double(12,8), vol int(14));
    insert into sd3_cleaned
    select date_format(date, '%Y-%m-%d'), time_format(date, '%H:%i:%s'),
    open, close, high, low, vol
    from sd3;
    
    create temporary table t1
    select distinct date, min(time) time, open
    from sd3_cleaned
	group by date;
    
    create temporary table t2
    select distinct date, time, close
    from sd3_cleaned
    where time = '16:00:00'
    group by date;
    
    create temporary table openclose AS
    select t1.date, t1.open, t2.close
    from t1
    inner join t2 On t1.date = t2.date;
     
    create temporary table openclosemax as
    select openclose.date, openclose.open, openclose.close, (abs(openclose.open-openclose.close)) as price_movement, concat(max(sd3_cleaned.high), ',', min(sd3_cleaned.low)) as Trading_Range
    from openclose
    left join sd3_cleaned on openclose.date = sd3_cleaned.date
    group by date
    order by price_movement desc
    limit 3;
    

    select openclosemax.date, openclosemax.open, openclosemax.close, openclosemax.price_movement, openclosemax.Trading_Range, sd3_cleaned.time
    from openclosemax
    left join sd3_cleaned on openclosemax.date = sd3_cleaned.date
    where time = max(sd3_cleaned.high) -- in progress, need to create new temp table for high price to find time at highest price 
    order by price_movement desc;
    
    select * from openclosemaxtime;
    
    drop table t1;
	drop table t2;
    drop table sd3_cleaned;
	drop table openclose;
	drop table openclosemax;
	drop table openclosemaxtime;
    
end //
delimiter ;